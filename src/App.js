import { useState, useEffect } from "react";
import axios from "axios";
import "./App.css";

function App() {
  const [data, setData] = useState("");

  useEffect(() => {
    // serviceWorker
    if ("serviceWorker" in navigator) {
      window.addEventListener("load", () => {
        navigator.serviceWorker.register("/serviceWorker.js");
      });
    }
    fetchData();
  }, []);

  const fetchData = () => {
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then((res) => {
        setData(res.data.slice(0, 10));
      })
      .catch((err) => console.error(`Error: ${err}`));
  };

  const renderRow = () => {
    if (data.length > 0) {
      return data.map((record, index) => {
        return (
          <>
            <tr>
              <td style={{ textAlign: "left" }}>{record.title}</td>
            </tr>
          </>
        );
      });
    } else {
      return (
        <>
          <tr>
            <td>&nbsp:</td>
          </tr>
        </>
      );
    }
  };

  return (
    <div className="App">
      <table>
        <thead>
          <tr>
            <th style={{ textAlign: "left" }}>Event</th>
          </tr>
        </thead>
        <tbody>{renderRow()}</tbody>
      </table>
    </div>
  );
}

export default App;
